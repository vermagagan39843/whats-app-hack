# Copyright 2022 Offensive Security
# SPDX-license-identifier: GPL-3.0-only

# WARNING!  The SSH client configuration is documented in ssh_config(5),
# however, the config might be parsed by other programs, and those might
# not support the syntax fully. For example:
# * libssh: doesn't support +,-,^ signs in version 0.10.x or below.
# * paramiko or related: no idea if/what is supported.
#
# Therefore, best to refrain from using syntax that it bleeding-edge or
# poorly documented (such as wildcards, cf. commit message in 823baff2).

import os

from kali_tweaks.utils import (
    run_as_root,
    say,
    write_file_as_root,
)

# ssh -Q Ciphers | sort -u | sed -e 's/^/    "/' -e 's/$/",/'
ALL_CIPHERS = [
    "3des-cbc",
    "aes128-cbc",
    "aes128-ctr",
    "aes128-gcm@openssh.com",
    "aes192-cbc",
    "aes192-ctr",
    "aes256-cbc",
    "aes256-ctr",
    "aes256-gcm@openssh.com",
    "chacha20-poly1305@openssh.com",
]

# Sorted per order of preference. Keep it sorted according to the defaults.
# ssh -F none -G '*' | grep ^hostkeyalgorithms | cut -d' ' -f2- | tr , '\n' > enabled
# sort -u < enabled > enabled.sorted
# ssh -Q hostkeyalgorithms | sort -u > all.sorted
# { cat enabled; comm -13 enabled.sorted all.sorted; } | sed -e 's/^/    "/' -e 's/$/",/'
ALL_HOST_KEY_ALGORITHMS = [
    "ssh-ed25519-cert-v01@openssh.com",
    "ecdsa-sha2-nistp256-cert-v01@openssh.com",
    "ecdsa-sha2-nistp384-cert-v01@openssh.com",
    "ecdsa-sha2-nistp521-cert-v01@openssh.com",
    "sk-ssh-ed25519-cert-v01@openssh.com",
    "sk-ecdsa-sha2-nistp256-cert-v01@openssh.com",
    "rsa-sha2-512-cert-v01@openssh.com",
    "rsa-sha2-256-cert-v01@openssh.com",
    "ssh-ed25519",
    "ecdsa-sha2-nistp256",
    "ecdsa-sha2-nistp384",
    "ecdsa-sha2-nistp521",
    "sk-ssh-ed25519@openssh.com",
    "sk-ecdsa-sha2-nistp256@openssh.com",
    "rsa-sha2-512",
    "rsa-sha2-256",
    "ssh-dss",
    "ssh-dss-cert-v01@openssh.com",
    "ssh-rsa",
    "ssh-rsa-cert-v01@openssh.com",
    "webauthn-sk-ecdsa-sha2-nistp256@openssh.com",
]

# ssh -Q KexAlgorithms | sort -u | sed -e 's/^/    "/' -e 's/$/",/'
ALL_KEX_ALGORITHMS = [
    "curve25519-sha256",
    "curve25519-sha256@libssh.org",
    "diffie-hellman-group14-sha1",
    "diffie-hellman-group14-sha256",
    "diffie-hellman-group16-sha512",
    "diffie-hellman-group18-sha512",
    "diffie-hellman-group1-sha1",
    "diffie-hellman-group-exchange-sha1",
    "diffie-hellman-group-exchange-sha256",
    "ecdh-sha2-nistp256",
    "ecdh-sha2-nistp384",
    "ecdh-sha2-nistp521",
    "sntrup761x25519-sha512@openssh.com",
]

# Sorted per order of preference. Keep it sorted according to the defaults.
# ssh -F none -G '*' | grep ^macs | cut -d' ' -f2- | tr , '\n' > enabled
# sort -u < enabled > enabled.sorted
# ssh -Q macs | sort -u > all.sorted
# { cat enabled; comm -13 enabled.sorted all.sorted; } | sed -e 's/^/    "/' -e 's/$/",/'
ALL_MACS = [
    "umac-64-etm@openssh.com",
    "umac-128-etm@openssh.com",
    "hmac-sha2-256-etm@openssh.com",
    "hmac-sha2-512-etm@openssh.com",
    "hmac-sha1-etm@openssh.com",
    "umac-64@openssh.com",
    "umac-128@openssh.com",
    "hmac-sha2-256",
    "hmac-sha2-512",
    "hmac-sha1",
    "hmac-md5",
    "hmac-md5-96",
    "hmac-md5-96-etm@openssh.com",
    "hmac-md5-etm@openssh.com",
    "hmac-sha1-96",
    "hmac-sha1-96-etm@openssh.com",
]

# ssh -Q PubkeyAcceptedAlgorithms | sort -u | sed -e 's/^/    "/' -e 's/$/",/'
ALL_PUBKEY_ACCEPTED_ALGORITHMS = [
    "ecdsa-sha2-nistp256",
    "ecdsa-sha2-nistp256-cert-v01@openssh.com",
    "ecdsa-sha2-nistp384",
    "ecdsa-sha2-nistp384-cert-v01@openssh.com",
    "ecdsa-sha2-nistp521",
    "ecdsa-sha2-nistp521-cert-v01@openssh.com",
    "rsa-sha2-256",
    "rsa-sha2-256-cert-v01@openssh.com",
    "rsa-sha2-512",
    "rsa-sha2-512-cert-v01@openssh.com",
    "sk-ecdsa-sha2-nistp256-cert-v01@openssh.com",
    "sk-ecdsa-sha2-nistp256@openssh.com",
    "sk-ssh-ed25519-cert-v01@openssh.com",
    "sk-ssh-ed25519@openssh.com",
    "ssh-dss",
    "ssh-dss-cert-v01@openssh.com",
    "ssh-ed25519",
    "ssh-ed25519-cert-v01@openssh.com",
    "ssh-rsa",
    "ssh-rsa-cert-v01@openssh.com",
    "webauthn-sk-ecdsa-sha2-nistp256@openssh.com",
]

KALI_SSH_WIDE_COMPAT_CONTENT = f"""\
# This file was added by kali-tweaks. Please use the kali-tweaks
# tool if you want to disable it.
#
# The configuration below enables legacy ciphers and algorithms,
# to allow interacting with old servers that still use those.

Host *
    Ciphers {','.join(ALL_CIPHERS)}
    KexAlgorithms {','.join(ALL_KEX_ALGORITHMS)}
    HostKeyAlgorithms {','.join(ALL_HOST_KEY_ALGORITHMS)}
    MACs {','.join(ALL_MACS)}
    PubkeyAcceptedAlgorithms {','.join(ALL_PUBKEY_ACCEPTED_ALGORITHMS)}
    LocalCommand /bin/echo "Warning: SSH client configured for wide compatibility by kali-tweaks."
    PermitLocalCommand yes
"""  # noqa


class SSHSetting:
    def __init__(self):
        self.kali_conf = "/etc/ssh/ssh_config.d/kali-wide-compat.conf"

    def load(self):
        fn = self.kali_conf
        if os.path.isfile(fn):
            value = "compat"
        else:
            value = "secure"
        return {"hardening": value}

    def apply(self, config):
        say("Configuring SSH")
        if "hardening" in config:
            value = config["hardening"]
            if value == "compat":
                self.set_wide_compat()
            elif value == "secure":
                self.unset_wide_compat()
            else:
                raise NotImplementedError(f"'{value}' not supported")

    def set_wide_compat(self):
        fn = self.kali_conf
        print("> Enabling wide compatibility")
        print(f"> Writing changes to {fn}")
        write_file_as_root(fn, KALI_SSH_WIDE_COMPAT_CONTENT)

    def unset_wide_compat(self):
        fn = self.kali_conf
        print("> Disabling wide compatibility")
        print(f"> Removing {fn}")
        run_as_root(f"rm -f {fn}", log=False)
